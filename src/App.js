import React, { Component } from 'react';
import LandingPage from './components/landingPage';
import Search from './components/search';
import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
class App extends Component {
  constructor() {
    super()
    this.state = {
      detailMod: false,
      searchContent: "",
      searchResult:[],
    }
  }
  setTitle = (input) => {
    this.setState({searchContent: input});
  }
  setResult = (result) => {
    this.setState({searchResult: result});
  }
  render() {
    return (
      <BrowserRouter basename = "/picular-clone">
          <Switch>
            <Route path="/" exact>
              <LandingPage setResult = {this.setResult} setTitle = {this.setTitle}></LandingPage>
            </Route>
            <Route path="/detail" exact>
              <Search searchResult = {this.state.searchResult} searchContent = {this.state.searchContent} setTitle = {this.setTitle} setResult = {this.setResult}></Search>
            </Route>
          </Switch>
      </BrowserRouter>
    ); 
  }
}

export default App;
