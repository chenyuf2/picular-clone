import React, { Component } from 'react';
import '../App.css';
import axios from 'axios';
import {
    Link,
    withRouter,
  } from "react-router-dom";
class LandingPage extends Component {
    state = {
        currentSearch: "",
        currentResult: [],
    }
    updateSearch = (e) => {
        // console.log(e.target.value);
          this.setState({
              currentSearch: e.target.value
          })
          console.log(e.target.value);
    }
    expandArea = (e) => {
        e.preventDefault();
        var textPlace = document.getElementById("feedback-form");
        document.getElementById("feedback-text").style.display = "block";
       document.getElementById("icon-button").style.display = "none";
        textPlace.style.animation =  "0.4s ease 0s 1 normal none running feedbackForm";
        textPlace.style.animationFillMode = "forwards";
    }

    closeArea = (e) => {
        e.preventDefault();
        var textPlace = document.getElementById("feedback-form");
        document.getElementById("feedback-text").style.display = "none";
       document.getElementById("icon-button").style.display = "block";
        textPlace.style.animation =  "0.4s ease 0s 1 normal none running closefeedbackForm";
        textPlace.style.animationFillMode = "forwards";
    }

    emojiClick = (e) => {
        e.preventDefault();
        var emoji = e.target.innerHTML;
        document.getElementById("feedback-text-area").value += emoji;
    }

    sendEmail = (e) => {
        e.preventDefault();
        
    }
    getRandomInt = (max) => {
        return Math.floor(Math.random() * Math.floor(max));
      }

    colorChange = (colors) => {
        var index = this.getRandomInt(colors.length);
        for (var i = 0; i < 18; i++) {
            let id = i;
            let currentId = "block" + id;
            let nextId = "block" + (id + 18)
            let currentColor = colors[index][this.getRandomInt(5)];
            if (document.getElementById(currentId) != null && document.getElementById(nextId) != null) {
                document.getElementById(currentId).style.backgroundColor = currentColor;
                document.getElementById(nextId).style.backgroundColor = currentColor;
            }
        }
        
    }

    searchClick = (e) => {
        e.preventDefault();
        this.props.setTitle(this.state.currentSearch);
        var inputKey = this.state.currentSearch.split(' ');
        var searchKey = "";
        for (var i = 0; i < inputKey.length; i++) {
            searchKey = searchKey.concat(inputKey[i]);
            if (i < inputKey.length - 1) {
                searchKey = searchKey.concat("+");
            }
        }
        var finalSearchKey = "https://pixabay.com/api/?key=16349355-24ee0f12ae24461685883bef1&q="+searchKey+"&image_type=photo&pretty=true";
        axios.get(finalSearchKey)
        .then(res => {
            console.log(res.data.hits);
            this.setState({currentResult: res.data.hits});
            this.props.setResult(res.data.hits);
            this.props.history.push("/detail");
            }
        );
    }
    render() {
        const grey = ["#f1f4f7"];
        const colorSet1 = ["#fe4a49", "#2ab7ca", "#fed766", "#e6e6ea", "#f4f4f8"];
        const colorSet2 = ["#eee3e7", "#ead5dc", "#eec9d2", "#f4b6c2", "#f6abb6"];
        const colorSet3 = ["#011f4b" , "#03396c" , "#005b96", "#6497b1", "#b3cde0"];
        const colorSet4 = ["#051e3e", "#251e3e", "#451e3e", "#651e3e", "#851e3e"];
        const colorSet5 = ["#4a4e4d", "#0e9aa7", "#3da4ab", "#f6cd61", "#fe8a71"];
        const colorSet6 = ["#2a4d69", "#4b86b4", "#adcbe3", "#e7eff6", "#63ace5"];
        const colorSet7 = ["#fe9c8f", "#feb2a8", "#fec8c1", "#fad9c1", "#f9caa7"];
        const colorSet8 = ["#009688", "#35a79c", "#54b2a9", "#65c3ba", "#83d0c9"];
        const colorSet9 = ["#ffe9dc","#fce9db", "#e0a899", "#dfa290", "#c99789"];
        const colorSet10 = ["#96ceb4","#ffeead", "#ff6f69", "#ffcc5c", "#88d8b0"];
        const colorSet11 = ["#a8e6cf", "#dcedc1", "#ffd3b6", "#ffaaa5", "#ff8b94"];
        const colorSet12 = ["#ebf4f6", "#bdeaee", "#76b4bd", "#58668b", "#5e5656"];
        const colorSet13 = ["#fff6e9", "#ffefd7", "#fffef9", "#e3f0ff", "#d2e7ff"];
        const colorSet14 = ["#edc951", "#eb6841", "#cc2a36", "#4f372d", "#00a0b0"];
        const colorSet15 = ["#84c1ff", "#add6ff", "#d6eaff", "#eaf4ff", "#f8fbff"];
        const colorSet16 = ["#2e003e", "#3d2352", "#3d1e6d", "#8874a3", "#e4dcf1"];
        const colorSet17 = ["#8d5524", "#c68642", "#e0ac69", "#f1c27d", "#ffdbac"];
        const colorSet18 = ["#343d46" , "#4f5b66", "#65737e", "#a7adba", "#c0c5ce"];
        const colorSet19 = ["#e3c9c9", "#f4e7e7", "#eedbdb", "#cecbcb", "#cbdadb"];
        var allColors = [];
        // allColors.push(grey);
        allColors.push(colorSet1);
        allColors.push(colorSet2);
        allColors.push(colorSet3);
        allColors.push(colorSet4);
        allColors.push(colorSet5);
        allColors.push(colorSet6);
        allColors.push(colorSet7);
        allColors.push(colorSet8);
        allColors.push(colorSet9);
        allColors.push(colorSet10);
        allColors.push(colorSet11);
        allColors.push(colorSet12);
        allColors.push(colorSet13);
        allColors.push(colorSet14);
        allColors.push(colorSet15);
        allColors.push(colorSet16);
        allColors.push(colorSet17);
        allColors.push(colorSet18);
        allColors.push(colorSet19);

        setInterval(() => this.colorChange(allColors), 7000);
        return (
            <div className = "outer row remove-margin" style={{height: '100vh', overflowY: 'hidden'}}>
                 <form className = "feedback" id="feedback-form">
                    <div className="feedback-text" id = "feedback-text">
                        <textarea className = "textarea-temp" id="feedback-text-area" placeholder="Write something here.. ✏️"></textarea>
                        <div className="emoji-list">
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>🔥</button>
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>😁</button>
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>❤️</button>
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>🙌</button>
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>😒</button>
                        </div>
                        <div className="send-form">
                            <button className="send-btn" onClick={this.sendEmail.bind(this)}>Send</button>
                            <button className="close-btn" onClick = {this.closeArea.bind(this)}>
                                <img className="close-img" alt="Thank you" src ="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-close-512.png"></img>
                            </button>
                        </div>
                    </div>
                    <button id = "icon-button" className="feedback-icon" onClick={this.expandArea.bind(this)}>
                        <img alt="Thank you" src ="https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/pencil.png"></img>
                        <img alt="Thank you" src ="https://icons.iconarchive.com/icons/google/noto-emoji-people-bodyparts/256/11959-victory-hand-icon.png" style={{display: "none"}}></img>
                    </button>
                </form>
                <div className = "col-sm-6 block-width" style={{padding: '5rem'}}>
                    <form className = "form-style" onSubmit = {this.searchClick.bind(this)} action="/detail">
                        <svg width="139" height="33" viewBox="0 0 464 107" xmlns="http://www.w3.org/2000/svg"><g fill-rule="evenodd" fill="#778A99"><path d="M0 35.5v42.361c3.513-4.098 9.072-6.63 14.157-6.861H35.5C55.106 71 71 55.106 71 35.5S55.106 0 35.5 0 0 15.894 0 35.5z" className="part__ABeGg" fill="#778A99"></path><path d="M30 80v12c0 8.284-6.716 15-15 15H3a3 3 0 0 1-3-3V92c0-8.284 6.716-15 15-15h12a3 3 0 0 1 3 3z" className="part__ABeGg" fill="#778A99"></path><path d="M128.124 66.563v22.739a2 2 0 0 1-2 2H113a2 2 0 0 1-2-2V22.776a2 2 0 0 1 2-2h25.74c8.411 0 14.82 2.078 19.227 6.234 4.406 4.157 6.609 9.76 6.609 16.809 0 4.39-.985 8.296-2.954 11.721-1.97 3.425-4.89 6.118-8.763 8.08-3.872 1.962-8.578 2.943-14.12 2.943h-10.615zm8.713-13.666c6.876 0 10.314-3.026 10.314-9.078 0-6.185-3.438-9.277-10.314-9.277h-8.713v18.355h8.713zm35.25-32.367c0-2.753.87-5.03 2.611-6.83 1.741-1.8 4.042-2.7 6.902-2.7 2.86 0 5.16.9 6.902 2.7 1.741 1.8 2.611 4.077 2.611 6.83 0 2.682-.87 4.923-2.611 6.723-1.741 1.8-4.042 2.7-6.902 2.7-2.86 0-5.16-.9-6.902-2.7-1.741-1.8-2.611-4.041-2.611-6.723zm18.025 17.11v51.662a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V37.639a2 2 0 0 1 2-2h13.124a2 2 0 0 1 2 2zm37.053-2.7c7.21 0 13.252 1.98 18.125 5.936 4.352 3.533 7.266 8.246 8.743 14.138a1.747 1.747 0 0 1-1.694 2.172h-14.451a2 2 0 0 1-1.895-1.36 5.788 5.788 0 0 0-.033-.096c-.61-1.706-1.573-3.083-2.887-4.13-1.67-1.33-3.772-1.995-6.309-1.995-3.004 0-5.474 1.18-7.41 3.54-1.937 2.362-2.905 5.803-2.905 10.325 0 4.523.968 7.964 2.904 10.325 1.937 2.36 4.407 3.541 7.411 3.541 2.537 0 4.64-.665 6.309-1.995 1.33-1.06 2.298-2.456 2.908-4.19l.015-.043a2 2 0 0 1 1.892-1.353h14.73a1.529 1.529 0 0 1 1.485 1.888c-1.454 6.025-4.392 10.832-8.813 14.422C240.417 90.022 234.375 92 227.165 92c-5.475 0-10.332-1.147-14.571-3.442-4.24-2.294-7.56-5.602-9.964-9.925-2.404-4.323-3.605-9.377-3.605-15.163 0-5.852 1.185-10.923 3.555-15.212 2.37-4.29 5.691-7.581 9.964-9.876 4.273-2.294 9.146-3.441 14.62-3.441zm91.83 2.7v51.662a2 2 0 0 1-2 2H303.77a2 2 0 0 1-2-2v-8.075c-1.602 3.258-3.989 5.852-7.16 7.78-3.171 1.929-6.86 2.893-11.066 2.893-6.409 0-11.5-2.128-15.271-6.384-3.772-4.256-5.658-10.108-5.658-17.557V37.64a2 2 0 0 1 2-2h13.024a2 2 0 0 1 2 2v28.226c0 3.79.985 6.733 2.954 8.828 1.97 2.095 4.623 3.142 7.961 3.142 3.472 0 6.21-1.097 8.212-3.292 2.003-2.194 3.004-5.32 3.004-9.377V37.64a2 2 0 0 1 2-2h13.225a2 2 0 0 1 2 2zm28.94-18.156v69.818a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V19.484a2 2 0 0 1 2-2h13.125a2 2 0 0 1 2 2zm33.047 15.457c4.273 0 7.945.964 11.016 2.893 3.071 1.928 5.34 4.555 6.81 7.88V37.64a2 2 0 0 1 2-2h13.024a2 2 0 0 1 2 2v51.663a2 2 0 0 1-2 2h-13.024a2 2 0 0 1-2-2v-8.075c-1.47 3.325-3.739 5.952-6.81 7.88-3.071 1.929-6.743 2.893-11.016 2.893-4.606 0-8.729-1.147-12.367-3.442-3.639-2.294-6.51-5.602-8.612-9.925-2.103-4.323-3.155-9.377-3.155-15.163 0-5.852 1.052-10.923 3.155-15.212 2.103-4.29 4.973-7.581 8.612-9.876 3.638-2.294 7.76-3.441 12.367-3.441zm5.508 14.963c-3.672 0-6.626 1.197-8.862 3.591-2.237 2.394-3.355 5.72-3.355 9.975 0 4.257 1.118 7.582 3.355 9.976 2.236 2.394 5.19 3.59 8.862 3.59 3.605 0 6.56-1.23 8.863-3.69s3.455-5.752 3.455-9.876c0-4.19-1.152-7.498-3.455-9.925-2.304-2.427-5.258-3.641-8.863-3.641zm58.283-3.79c2.136-3.392 4.89-6.086 8.261-8.08 2.79-1.651 5.773-2.62 8.951-2.904A1.85 1.85 0 0 1 464 36.972v14.324a2 2 0 0 1-2 2h-2.907c-4.54 0-8.061.964-10.565 2.892-2.503 1.929-3.755 5.188-3.755 9.776v23.338a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V37.639a2 2 0 0 1 2-2h13.124a2 2 0 0 1 2 2v8.474z"></path></g></svg>
                        <div className="center search-bar">
                            <h2>Google, but for colors.</h2>
                            <label for="search">Search text</label>
                            <i className = "fa fa-search fa-3x search-icon"></i>
                            
                            <input className="search" type="search" name="search" id="search" placeholder="winter" required="" minlength="2" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="true" onChange={this.updateSearch}></input>
        
                        </div>
                        <div className = "subtitle">
                            <Link to="/detail" style={{color: '#778a99'}}>
                            <a className = "subtitle-font"> 
                                Crafted with <span role="img">❤️</span> by <span className="fa-stack">
                                    <i className="fa fa-circle fa-stack-1x" style={{fontSize: '2em'}}></i>
                                    <i className="fa fa-rocket fa-stack-1x" style={{color: 'white'}}></i>
                                </span>
                            </a>
                            </Link>
                        </div>
                    </form>    
                </div>
                <div className = "color-outer">
                    <div className = "color-inner">
                        <div className = "grid-block card-columns">
                            <div id="block0" className = "span-9 red-color card-contains"></div>
                            <div  id="block1"className = "span-7 red-color card-contains"></div>
                            <div id="block2" className = "span-5 red-color card-contains"></div>
                            <div id="block3" className = "span-7 red-color card-contains"></div>
                            <div id="block4" className = "span-5 red-color card-contains"></div>
                            <div id="block5" className = " span-9 red-color card-contains"></div>
                            <div id="block6" className = "span-5 red-color card-contains"></div>
                            <div id="block7" className = " span-7 red-color card-contains"></div>
                            <div id="block8" className = " span-9 red-color card-contains"></div>
                            <div id="block9" className = "span-9 red-color card-contains"></div>
                            <div id="block10" className = "span-7 red-color card-contains"></div>
                            <div id="block11" className = "span-5 red-color card-contains"></div>
                            <div id="block12" className = "span-7 red-color card-contains"></div>
                            <div id="block13" className = "span-5 red-color card-contains"></div>
                            <div id="block14" className = " span-9 red-color card-contains"></div>
                            <div id="block15" className = "span-5 red-color card-contains"></div>
                            <div id="block16" className = " span-7 red-color card-contains"></div>
                            <div id="block17" className = " span-9 red-color card-contains"></div>
                        </div>
                        <div className = "grid-block card-columns">
                            <div id="block18" className = "span-9 red-color card-contains"></div>
                            <div id="block19" className = "span-7 red-color card-contains"></div>
                            <div id="block20" className = "span-5 red-color card-contains"></div>
                            <div id="block21" className = "span-7 red-color card-contains"></div>
                            <div id="block22" className = "span-5 red-color card-contains"></div>
                            <div id="block23" className = " span-9 red-color card-contains"></div>
                            <div id="block24" className = "span-5 red-color card-contains"></div>
                            <div id="block25" className = " span-7 red-color card-contains"></div>
                            <div id="block26" className = " span-9 red-color card-contains"></div>
                            <div id="block27" className = "span-9 red-color card-contains"></div>
                            <div id="block28" className = "span-7 red-color card-contains"></div>
                            <div id="block29" className = "span-5 red-color card-contains"></div>
                            <div id="block30" className = "span-7 red-color card-contains"></div>
                            <div id="block31" className = "span-5 red-color card-contains"></div>
                            <div id="block32" className = " span-9 red-color card-contains"></div>
                            <div id="block33" className = "span-5 red-color card-contains"></div>
                            <div id="block34" className = " span-7 red-color card-contains"></div>
                            <div id="block35" className = " span-9 red-color card-contains"></div>

                          
                            
                            
                        </div>
                    </div>
                </div>
               
            </div>
        )

    }

}

export default withRouter(LandingPage);