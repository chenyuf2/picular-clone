import React, { Component } from 'react';
import '../App.css';
import axios from 'axios';
import {Palette} from 'react-palette';
import ImagePalette from 'react-image-palette';
import {
    EmailShareButton,
    FacebookShareButton,
    PinterestShareButton,
    RedditShareButton,
    TwitterShareButton,
    EmailIcon,
    FacebookIcon,
    RedditIcon,
    TwitterIcon,
    PinterestIcon,
  } from "react-share";
export default class Search extends Component {
    state = {
        currentSearch: this.props.searchContent,
        currentResult: this.props.searchResult,
        pinColors:[],
    }
    updateSearch = (e) => {
        // console.log(e.target.value);
          this.setState({
              currentSearch: e.target.value
          })
    }
    startSearch = (e) => {
        e.preventDefault();
        if (this.state.currentSearch.length < 2) {
            alert("Please type in more than 2 characters");
            return;
        }
        // console.log(document.getElementById.val);
        document.getElementById("bigSearch").innerHTML = this.state.currentSearch;
        var inputKey = this.state.currentSearch.split(' ');
        var searchKey = "";
        for (var i = 0; i < inputKey.length; i++) {
            searchKey = searchKey.concat(inputKey[i]);
            if (i < inputKey.length - 1) {
                searchKey = searchKey.concat("+");
            }
        }
        var finalSearchKey = "https://pixabay.com/api/?key=16349355-24ee0f12ae24461685883bef1&q="+searchKey+"&image_type=photo&pretty=true";
        axios.get(finalSearchKey)
        .then(res => {
            this.setState({currentResult: res.data.hits});
            }
        );

        if (this.state.pinColors.length != 0) {
            for (var i = 0; i < Math.min(17, this.state.currentResult.length); i++) {
                let index = i;
                let tempId = "p" + index;
                let ele = document.getElementById(tempId);
                if (ele !== undefined) {
                    ele.style.color = "";
                    var pinId = "pin" + index;
                    var unpinId = "unpin" + index;
                    var pinElement = document.getElementById(pinId);
                    var unpinElement = document.getElementById(unpinId);
                    pinElement.style = "";
                    unpinElement.style = "";
                } 
            }
        }
        // const result = analyze('https://media-exp1.licdn.com/dms/image/C4E0BAQGSFusG5WQ2MA/company-logo_200_200/0?e=2159024400&v=beta&t=vKEwlO_24FQXeV4qfSoHD0Xomi7DVkxLdg6b6_97mo8')
        // .then(res => {
        //     console.log(res[0].color);
        // }) // also supports base64 encoded image strings

        
    }
    SomeComponent = ( image ) => (
        <ImagePalette image={image}>
          {({ backgroundColor, color, alternativeColor }) => (
            <div style={{ backgroundColor: color }}>
              This div has been themed based on
            </div>
          )}
        </ImagePalette>
      )    

    copyClick = (e) => {
        var id = e.target.id.substring(1, e.target.id.length);
        var newId = "c" + id;
        var element = document.getElementById(newId);
        element.style = "";
        var textField = document.createElement('textarea');
        textField.innerText = e.target.innerHTML;
        document.body.appendChild(textField);
        textField.select();
        document.execCommand('copy')
        textField.remove();
        element.style.animation = '1.5s ease 0s 1 normal none running opacityAnimation';
    }

    expandArea = (e) => {
        e.preventDefault();
        var textPlace = document.getElementById("feedback-form");
        document.getElementById("feedback-text").style.display = "block";
       document.getElementById("icon-button").style.display = "none";
        textPlace.style.animation =  "0.4s ease 0s 1 normal none running feedbackForm";
        textPlace.style.animationFillMode = "forwards";
    }

    closeArea = (e) => {
        e.preventDefault();
        var textPlace = document.getElementById("feedback-form");
        document.getElementById("feedback-text").style.display = "none";
       document.getElementById("icon-button").style.display = "block";
        textPlace.style.animation =  "0.4s ease 0s 1 normal none running closefeedbackForm";
        textPlace.style.animationFillMode = "forwards";
    }

    emojiClick = (e) => {
        e.preventDefault();
        var emoji = e.target.innerHTML;
        document.getElementById("feedback-text-area").value += emoji;
    }

    sendEmail = (e) => {
        e.preventDefault();
        
    }
    shadeColor = (color, percent) => {

        var R = parseInt(color.substring(1,3),16);
        var G = parseInt(color.substring(3,5),16);
        var B = parseInt(color.substring(5,7),16);
    
        R = parseInt(R * (100 + percent) / 100);
        G = parseInt(G * (100 + percent) / 100);
        B = parseInt(B * (100 + percent) / 100);
    
        R = (R<255)?R:255;  
        G = (G<255)?G:255;  
        B = (B<255)?B:255;  
    
        var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
        var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
        var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));
    
        return "#"+RR+GG+BB;
    }
    closePin = (id,e) => {
        e.preventDefault();
        var currentColor = this.state.pinColors[id];
        for (var j = 0; j < Math.min(17, this.state.currentResult.length); j++) {
            let tempId = "p" + j;
            var tempElement = document.getElementById(tempId);
            // console.log(document.getElementById("b" + j).innerHTML);
            // console.log(currentColor);
            if (tempElement !== undefined && tempElement.style.color == "white" && document.getElementById("b" + j).innerHTML === currentColor) {
                console.log("hello");
                tempElement.style.color = "";
                break;
            }
        }
        this.state.pinColors.splice(id, 1);
        for (var i = 0; i < 8; i++) {
            let index = i;
            if (index < this.state.pinColors.length) {
                let currentColor = this.state.pinColors[index];
                document.getElementById("k" + index).style.display = "flex";
                document.getElementById("k" + index).style.backgroundColor = currentColor;
                document.getElementById("m" + index).innerHTML = currentColor;
            } else {
                document.getElementById("k" + index).style.display = "none";
                document.getElementById("k" + index).style.backgroundColor = "";
            }
        }
        if (this.state.pinColors.length === 0) {
            document.getElementById("pin-block").style.display = "none";
            document.getElementById("closeshare").style.display = "inline-block";
            document.getElementById("openshare").style.display = "none";
        }
        // this.state.pinColors.

    }
    pinClick = (id, color, e) => {
        e.preventDefault();
        var element = document.getElementById("p" + id);
        var pinId = "pin" + id;
        var unpinId = "unpin" + id;
        var pinElement = document.getElementById(pinId);
        var unpinElement = document.getElementById(unpinId);
        pinElement.style = "";
        unpinElement.style = "";
        //for pin button click
        if (element.style.color !== "white") {//pin is not clicked before, we want to save the color

            if (this.state.pinColors.length == 0) {
                document.getElementById("pin-block").style.display = "grid";
                document.getElementById("closeshare").style.display = "none";
                document.getElementById("openshare").style.display = "inline-block";

            }
            element.style.color = "white";
            if (this.state.pinColors.length >= 8) {//more than 8 colors, replace the last color in array
                var lastColor = this.state.pinColors[7];
                for (var j = 0; j < Math.min(17, this.state.currentResult.length); j++) {
                    let tempId = "p" + j;
                    var tempElement = document.getElementById(tempId);
                    if (tempElement !== undefined && tempElement.style.color ==="white" && document.getElementById("b" + j).innerHTML === lastColor) {
                        tempElement.style.color = "";
                        break;
                    }
                }
                this.state.pinColors.splice(7, 1, color);
            } else {
                this.state.pinColors.push(color);
            }
            pinElement.style.animation = '1.5s ease 0s 1 normal none running opacityAnimation';
        } else { //release the color


            element.style.color = ""; //change pin-button to white
            for (var i = 0; i < this.state.pinColors.length; i++) {
                let index = i;
                if (this.state.pinColors[index] === color) {
                    this.state.pinColors.splice(index, 1);
                }
            }
            if (this.state.pinColors.length === 0) {
                document.getElementById("pin-block").style.display = "none";
                document.getElementById("closeshare").style.display = "inline-block";
                document.getElementById("openshare").style.display = "none";
            }
           
            unpinElement.style.animation = '1.5s ease 0s 1 normal none running opacityAnimation';
        }
        // var pinHtml = "";
        for (var i = 0; i < 8; i++) {
            let index = i;
            if (index < this.state.pinColors.length) {
                let currentColor = this.state.pinColors[index];
                document.getElementById("k" + index).style.display = "flex";
                document.getElementById("k" + index).style.backgroundColor = currentColor;
                document.getElementById("m" + index).innerHTML = currentColor;
            } else {
                document.getElementById("k" + index).style.display = "none";
                document.getElementById("k" + index).style.backgroundColor = "";
            }
        }
        
    }
    render() {
        var item = [];
        var iconHtml = <a href="/picular-clone"><svg width="139" height="33" viewBox="0 0 464 107" xmlns="http://www.w3.org/2000/svg"><g fillRule="evenodd" fill="#778A99"><path d="M0 35.5v42.361c3.513-4.098 9.072-6.63 14.157-6.861H35.5C55.106 71 71 55.106 71 35.5S55.106 0 35.5 0 0 15.894 0 35.5z" className="part__ABeGg" fill="#778A99" id="largeCircle" style={{transition: "1s"}}></path><path d="M30 80v12c0 8.284-6.716 15-15 15H3a3 3 0 0 1-3-3V92c0-8.284 6.716-15 15-15h12a3 3 0 0 1 3 3z" className="part__ABeGg" fill="#778A99" id="smallCircle" style={{transition: "1s"}}></path><path d="M128.124 66.563v22.739a2 2 0 0 1-2 2H113a2 2 0 0 1-2-2V22.776a2 2 0 0 1 2-2h25.74c8.411 0 14.82 2.078 19.227 6.234 4.406 4.157 6.609 9.76 6.609 16.809 0 4.39-.985 8.296-2.954 11.721-1.97 3.425-4.89 6.118-8.763 8.08-3.872 1.962-8.578 2.943-14.12 2.943h-10.615zm8.713-13.666c6.876 0 10.314-3.026 10.314-9.078 0-6.185-3.438-9.277-10.314-9.277h-8.713v18.355h8.713zm35.25-32.367c0-2.753.87-5.03 2.611-6.83 1.741-1.8 4.042-2.7 6.902-2.7 2.86 0 5.16.9 6.902 2.7 1.741 1.8 2.611 4.077 2.611 6.83 0 2.682-.87 4.923-2.611 6.723-1.741 1.8-4.042 2.7-6.902 2.7-2.86 0-5.16-.9-6.902-2.7-1.741-1.8-2.611-4.041-2.611-6.723zm18.025 17.11v51.662a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V37.639a2 2 0 0 1 2-2h13.124a2 2 0 0 1 2 2zm37.053-2.7c7.21 0 13.252 1.98 18.125 5.936 4.352 3.533 7.266 8.246 8.743 14.138a1.747 1.747 0 0 1-1.694 2.172h-14.451a2 2 0 0 1-1.895-1.36 5.788 5.788 0 0 0-.033-.096c-.61-1.706-1.573-3.083-2.887-4.13-1.67-1.33-3.772-1.995-6.309-1.995-3.004 0-5.474 1.18-7.41 3.54-1.937 2.362-2.905 5.803-2.905 10.325 0 4.523.968 7.964 2.904 10.325 1.937 2.36 4.407 3.541 7.411 3.541 2.537 0 4.64-.665 6.309-1.995 1.33-1.06 2.298-2.456 2.908-4.19l.015-.043a2 2 0 0 1 1.892-1.353h14.73a1.529 1.529 0 0 1 1.485 1.888c-1.454 6.025-4.392 10.832-8.813 14.422C240.417 90.022 234.375 92 227.165 92c-5.475 0-10.332-1.147-14.571-3.442-4.24-2.294-7.56-5.602-9.964-9.925-2.404-4.323-3.605-9.377-3.605-15.163 0-5.852 1.185-10.923 3.555-15.212 2.37-4.29 5.691-7.581 9.964-9.876 4.273-2.294 9.146-3.441 14.62-3.441zm91.83 2.7v51.662a2 2 0 0 1-2 2H303.77a2 2 0 0 1-2-2v-8.075c-1.602 3.258-3.989 5.852-7.16 7.78-3.171 1.929-6.86 2.893-11.066 2.893-6.409 0-11.5-2.128-15.271-6.384-3.772-4.256-5.658-10.108-5.658-17.557V37.64a2 2 0 0 1 2-2h13.024a2 2 0 0 1 2 2v28.226c0 3.79.985 6.733 2.954 8.828 1.97 2.095 4.623 3.142 7.961 3.142 3.472 0 6.21-1.097 8.212-3.292 2.003-2.194 3.004-5.32 3.004-9.377V37.64a2 2 0 0 1 2-2h13.225a2 2 0 0 1 2 2zm28.94-18.156v69.818a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V19.484a2 2 0 0 1 2-2h13.125a2 2 0 0 1 2 2zm33.047 15.457c4.273 0 7.945.964 11.016 2.893 3.071 1.928 5.34 4.555 6.81 7.88V37.64a2 2 0 0 1 2-2h13.024a2 2 0 0 1 2 2v51.663a2 2 0 0 1-2 2h-13.024a2 2 0 0 1-2-2v-8.075c-1.47 3.325-3.739 5.952-6.81 7.88-3.071 1.929-6.743 2.893-11.016 2.893-4.606 0-8.729-1.147-12.367-3.442-3.639-2.294-6.51-5.602-8.612-9.925-2.103-4.323-3.155-9.377-3.155-15.163 0-5.852 1.052-10.923 3.155-15.212 2.103-4.29 4.973-7.581 8.612-9.876 3.638-2.294 7.76-3.441 12.367-3.441zm5.508 14.963c-3.672 0-6.626 1.197-8.862 3.591-2.237 2.394-3.355 5.72-3.355 9.975 0 4.257 1.118 7.582 3.355 9.976 2.236 2.394 5.19 3.59 8.862 3.59 3.605 0 6.56-1.23 8.863-3.69s3.455-5.752 3.455-9.876c0-4.19-1.152-7.498-3.455-9.925-2.304-2.427-5.258-3.641-8.863-3.641zm58.283-3.79c2.136-3.392 4.89-6.086 8.261-8.08 2.79-1.651 5.773-2.62 8.951-2.904A1.85 1.85 0 0 1 464 36.972v14.324a2 2 0 0 1-2 2h-2.907c-4.54 0-8.061.964-10.565 2.892-2.503 1.929-3.755 5.188-3.755 9.776v23.338a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V37.639a2 2 0 0 1 2-2h13.124a2 2 0 0 1 2 2v8.474z"></path></g></svg></a>;
        if (this.state.currentResult.length >= 1) {
            var imgUrl = this.state.currentResult[0].largeImageURL;
            iconHtml = <Palette src={imgUrl}>
            {({ data, loading, error }) => {
                var colorOf = data.vibrant;
                if (data.vibrant !== undefined) {
                    colorOf = this.shadeColor(data.vibrant, 20);
                }

                return (<a href="/picular-clone"><svg width="139" height="33" viewBox="0 0 464 107" xmlns="http://www.w3.org/2000/svg"><g fillRule="evenodd" fill="#778A99"><path d="M0 35.5v42.361c3.513-4.098 9.072-6.63 14.157-6.861H35.5C55.106 71 71 55.106 71 35.5S55.106 0 35.5 0 0 15.894 0 35.5z" className="part__ABeGg" fill={data.vibrant} id="largeCircle" style={{transition: "1.5s"}}></path><path d="M30 80v12c0 8.284-6.716 15-15 15H3a3 3 0 0 1-3-3V92c0-8.284 6.716-15 15-15h12a3 3 0 0 1 3 3z" className="part__ABeGg" fill={colorOf} id="smallCircle" style={{transition: "1.5s"}}></path><path d="M128.124 66.563v22.739a2 2 0 0 1-2 2H113a2 2 0 0 1-2-2V22.776a2 2 0 0 1 2-2h25.74c8.411 0 14.82 2.078 19.227 6.234 4.406 4.157 6.609 9.76 6.609 16.809 0 4.39-.985 8.296-2.954 11.721-1.97 3.425-4.89 6.118-8.763 8.08-3.872 1.962-8.578 2.943-14.12 2.943h-10.615zm8.713-13.666c6.876 0 10.314-3.026 10.314-9.078 0-6.185-3.438-9.277-10.314-9.277h-8.713v18.355h8.713zm35.25-32.367c0-2.753.87-5.03 2.611-6.83 1.741-1.8 4.042-2.7 6.902-2.7 2.86 0 5.16.9 6.902 2.7 1.741 1.8 2.611 4.077 2.611 6.83 0 2.682-.87 4.923-2.611 6.723-1.741 1.8-4.042 2.7-6.902 2.7-2.86 0-5.16-.9-6.902-2.7-1.741-1.8-2.611-4.041-2.611-6.723zm18.025 17.11v51.662a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V37.639a2 2 0 0 1 2-2h13.124a2 2 0 0 1 2 2zm37.053-2.7c7.21 0 13.252 1.98 18.125 5.936 4.352 3.533 7.266 8.246 8.743 14.138a1.747 1.747 0 0 1-1.694 2.172h-14.451a2 2 0 0 1-1.895-1.36 5.788 5.788 0 0 0-.033-.096c-.61-1.706-1.573-3.083-2.887-4.13-1.67-1.33-3.772-1.995-6.309-1.995-3.004 0-5.474 1.18-7.41 3.54-1.937 2.362-2.905 5.803-2.905 10.325 0 4.523.968 7.964 2.904 10.325 1.937 2.36 4.407 3.541 7.411 3.541 2.537 0 4.64-.665 6.309-1.995 1.33-1.06 2.298-2.456 2.908-4.19l.015-.043a2 2 0 0 1 1.892-1.353h14.73a1.529 1.529 0 0 1 1.485 1.888c-1.454 6.025-4.392 10.832-8.813 14.422C240.417 90.022 234.375 92 227.165 92c-5.475 0-10.332-1.147-14.571-3.442-4.24-2.294-7.56-5.602-9.964-9.925-2.404-4.323-3.605-9.377-3.605-15.163 0-5.852 1.185-10.923 3.555-15.212 2.37-4.29 5.691-7.581 9.964-9.876 4.273-2.294 9.146-3.441 14.62-3.441zm91.83 2.7v51.662a2 2 0 0 1-2 2H303.77a2 2 0 0 1-2-2v-8.075c-1.602 3.258-3.989 5.852-7.16 7.78-3.171 1.929-6.86 2.893-11.066 2.893-6.409 0-11.5-2.128-15.271-6.384-3.772-4.256-5.658-10.108-5.658-17.557V37.64a2 2 0 0 1 2-2h13.024a2 2 0 0 1 2 2v28.226c0 3.79.985 6.733 2.954 8.828 1.97 2.095 4.623 3.142 7.961 3.142 3.472 0 6.21-1.097 8.212-3.292 2.003-2.194 3.004-5.32 3.004-9.377V37.64a2 2 0 0 1 2-2h13.225a2 2 0 0 1 2 2zm28.94-18.156v69.818a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V19.484a2 2 0 0 1 2-2h13.125a2 2 0 0 1 2 2zm33.047 15.457c4.273 0 7.945.964 11.016 2.893 3.071 1.928 5.34 4.555 6.81 7.88V37.64a2 2 0 0 1 2-2h13.024a2 2 0 0 1 2 2v51.663a2 2 0 0 1-2 2h-13.024a2 2 0 0 1-2-2v-8.075c-1.47 3.325-3.739 5.952-6.81 7.88-3.071 1.929-6.743 2.893-11.016 2.893-4.606 0-8.729-1.147-12.367-3.442-3.639-2.294-6.51-5.602-8.612-9.925-2.103-4.323-3.155-9.377-3.155-15.163 0-5.852 1.052-10.923 3.155-15.212 2.103-4.29 4.973-7.581 8.612-9.876 3.638-2.294 7.76-3.441 12.367-3.441zm5.508 14.963c-3.672 0-6.626 1.197-8.862 3.591-2.237 2.394-3.355 5.72-3.355 9.975 0 4.257 1.118 7.582 3.355 9.976 2.236 2.394 5.19 3.59 8.862 3.59 3.605 0 6.56-1.23 8.863-3.69s3.455-5.752 3.455-9.876c0-4.19-1.152-7.498-3.455-9.925-2.304-2.427-5.258-3.641-8.863-3.641zm58.283-3.79c2.136-3.392 4.89-6.086 8.261-8.08 2.79-1.651 5.773-2.62 8.951-2.904A1.85 1.85 0 0 1 464 36.972v14.324a2 2 0 0 1-2 2h-2.907c-4.54 0-8.061.964-10.565 2.892-2.503 1.929-3.755 5.188-3.755 9.776v23.338a2 2 0 0 1-2 2h-13.124a2 2 0 0 1-2-2V37.639a2 2 0 0 1 2-2h13.124a2 2 0 0 1 2 2v8.474z"></path></g></svg></a>)
              
            }}
            </Palette>
        }



        for (var i = 0; i < Math.min(17, this.state.currentResult.length); i++) {
            let currentData = this.state.currentResult[i];
            let imageUrl = currentData.largeImageURL;
            let h = currentData.imageHeight;
            let w = currentData.imageWidth;
            let tempClassKey = "";
            if (w > h) {
                tempClassKey = "img-block-common img-block-5";
            } else if (w <= h && h <= 1.3 * w) {
                tempClassKey = "img-block-common img-block-7";
            } else if (h > 1.3 * w && h <= 1.7 * w) {
                tempClassKey = "img-block-common img-block-9";
            } else if (h > 1.7 * w && h <= 1.9 * w) {
                tempClassKey = "img-block-common img-block-11";
            } else {
                tempClassKey = "img-block-common img-block-13";
            }
            let id = i;
            let colorId = "c" + id;
            let buttonId = 'b' + id;
            let pinId = 'p' + id;
            let pinnedId = 'pin' + id;
            let unpinnedId = "unpin" + id; 
                item.push(
                    <Palette src={imageUrl}>
                            {({ data, loading, error }) => {
                                return (
                                    <li className = {tempClassKey} style={{backgroundColor: data.vibrant}}>
                                        <div className="img-content">
                                        <div className="color-copy" id = {unpinnedId}>Unpinned.</div>
                                        <div className="color-copy" id = {pinnedId}>Pinned!</div>
                                        <div className="color-copy" id = {colorId}>HEX copied!</div>
                                        <button className="hex-button" id = {buttonId} onClick={this.copyClick.bind(this)}>{data.vibrant}</button>
                                        <button className="pin-button" id={pinId} onClick={this.pinClick.bind(this, id, data.vibrant)}>
                                        <svg aria-hidden="true" width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M12.966 12.08c.667.775.861 1.86.491 2.825l-.098.256a1 1 0 0 1-1.641.349l-.879-.879-1.88-1.88-7.696 7.043a.66.66 0 0 1-.912-.02.668.668 0 0 1-.026-.918l6.953-7.786-1.91-1.91-.878-.878a1 1 0 0 1 .35-1.64l.256-.099a2.797 2.797 0 0 1 2.824.491l4.323-3.602a2.522 2.522 0 0 1-.135-2.381l.142-.304a1 1 0 0 1 1.613-.285l5.676 5.675a1 1 0 0 1-.286 1.613l-.304.142c-.777.362-1.671.3-2.38-.135l-3.603 4.323z" fill="currentColor"></path></svg>
                                        </button>
                                        <span className="image-button">
                                            <button><i className="fa fa-sm fa-image"></i></button>
                                            <figure className="figure-detail">
                                                <img className="img-color-style" src={imageUrl}></img>
                                            </figure>
                                        </span>
                                        </div>
                                 
                                    </li>
                            )}}
                    </Palette>
                );
             
        }
        //append pin-colors

        
        return (
            <div className = "detail-block">
                <form className = "feedback" id="feedback-form">
                    <div className="feedback-text" id = "feedback-text">
                        <textarea className = "textarea-temp" id="feedback-text-area" placeholder="Write something here.. ✏️"></textarea>
                        <div className="emoji-list">
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>🔥</button>
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>😁</button>
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>❤️</button>
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>🙌</button>
                            <button type="button" className="emoji-btn" onClick = {this.emojiClick.bind(this)}>😒</button>
                        </div>
                        <div className="send-form">
                            <button className="send-btn" onClick={this.sendEmail.bind(this)}>Send</button>
                            <button className="close-btn" onClick = {this.closeArea.bind(this)}>
                                <img className="close-img" alt="Thank you" src ="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-close-512.png"></img>
                            </button>
                        </div>
                    </div>
                    <button id = "icon-button" className="feedback-icon" onClick={this.expandArea.bind(this)}>
                        <img alt="Thank you" src ="https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/pencil.png"></img>
                        <img alt="Thank you" src ="https://icons.iconarchive.com/icons/google/noto-emoji-people-bodyparts/256/11959-victory-hand-icon.png" style={{display: "none"}}></img>
                    </button>
                </form>
                <form className="search-form" onSubmit ={this.startSearch.bind(this)}>
                    {iconHtml}
                    <div className = "search-bar-block">
                        <label>Search Text</label>
                        <i className = "fa fa-search fa-2x search-detail-icon"></i>
                        <input className="search-bar-detail" type="search" name="search" id="search" placeholder="forest, water, desert and so on..." required="" minLength="2" autoComplete="off" autoCorrect="off" autoCapitalize="off" spellCheck="true" onChange = {this.updateSearch}></input>
                        <button type="submit">Search</button>
                    </div>
                    <a className = "subtitle-font"> 
                        Crafted with <span role="img">❤️</span> by <span className="fa-stack">
                            <i className="fa fa-circle fa-stack-1x" style={{fontSize: '2em'}}></i>
                            <i className="fa fa-rocket fa-stack-1x" style={{color: 'white'}}></i>
                        </span>
                    </a>
                </form>
                <div className="search-title">
                    <h1 id="bigSearch">{this.props.searchContent}</h1>
                    <section className="pin-section">
                        <div className="pin-title" id="closeshare">
                            <h4>
                            <small>New</small>
                            Pin your favorites!
                            <i className = "fa fa-sm fa-heart icon-space"></i>
                            </h4>
                        </div>
                        <div className="pin-title" id="openshare" style={{display: "none"}}>
                            <h4>
                            <small>New</small>
                            Share your palette!
                            <TwitterShareButton url="https://chenyuf2.gitlab.io/picular-clone/" className="ml-4 mr-4">
                                <TwitterIcon size={28} round={true}></TwitterIcon>
                            </TwitterShareButton>
                            <FacebookShareButton url="https://chenyuf2.gitlab.io/picular-clone/" className="mr-4">
                                <FacebookIcon size={28} round={true}></FacebookIcon>
                            </FacebookShareButton>
                            <PinterestShareButton url="https://chenyuf2.gitlab.io/picular-clone/" className="mr-4">
                                <PinterestIcon size={28} round={true}></PinterestIcon>
                            </PinterestShareButton>
                            <EmailShareButton url="https://chenyuf2.gitlab.io/picular-clone/">
                                <EmailIcon size={28} round={true}></EmailIcon>
                            </EmailShareButton>
                            
                            </h4>
                        </div>
                    </section>
                </div>
                <div className="pin-grid" id="pin-block">
                    <div className="pin-block" id="k0">
                        <span id="m0">'+currentColor+'</span>
                        <button className="delete-pin" onClick = {this.closePin.bind(this, 0)}>
                            <svg aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <path fill="#f3f3f3" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"></path>
                            </svg>
                            </button>
                    </div>
                    <div className="pin-block" id="k1">
                        <span id="m1">'+currentColor+'</span>
                        <button id='' className="delete-pin" onClick = {this.closePin.bind(this, 1)}>
                            <svg aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <path fill="#f3f3f3" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"></path>
                            </svg>
                            </button>
                    </div>
                    <div className="pin-block" id="k2">
                        <span id="m2">'+currentColor+'</span>
                        <button id='' className="delete-pin" onClick = {this.closePin.bind(this, 2)}>
                            <svg aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <path fill="#f3f3f3" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"></path>
                            </svg>
                            </button>
                    </div>
                    <div className="pin-block" id="k3">
                        <span id="m3">'+currentColor+'</span>
                        <button id='' className="delete-pin" onClick = {this.closePin.bind(this, 3)}>
                            <svg aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <path fill="#f3f3f3" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"></path>
                            </svg>
                            </button>
                    </div>
                    <div className="pin-block" id="k4">
                        <span id="m4">'+currentColor+'</span>
                        <button id='' className="delete-pin" onClick = {this.closePin.bind(this, 4)}>
                            <svg aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <path fill="#f3f3f3" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"></path>
                            </svg>
                            </button>
                    </div>
                    <div className="pin-block" id="k5">
                        <span id="m5">'+currentColor+'</span>
                        <button id='' className="delete-pin" onClick = {this.closePin.bind(this, 5)}>
                            <svg aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <path fill="#f3f3f3" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"></path>
                            </svg>
                            </button>
                    </div>
                    <div className="pin-block" id="k6">
                        <span id="m6">'+currentColor+'</span>
                        <button id='' className="delete-pin" onClick = {this.closePin.bind(this, 6)}>
                            <svg aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <path fill="#f3f3f3" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"></path>
                            </svg>
                            </button>
                    </div>
                    <div className="pin-block" id="k7">
                        <span id="m7">'+currentColor+'</span>
                        <button id='' className="delete-pin" onClick = {this.closePin.bind(this, 7)}>
                            <svg aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <path fill="#f3f3f3" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"></path>
                            </svg>
                            </button>
                    </div>
                </div>
                <ul className="img-group">
                    {item}
                </ul>
                
            </div>
        )
    }
}

//asdasd

